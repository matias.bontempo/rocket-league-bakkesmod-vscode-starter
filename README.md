choco install llvm
cmake
@id:ms-vscode.cmake-tools

https://chocolatey.org/packages/visualcpp-build-tools
choco install visualcpp-build-tools

### error MSB4025: The project file could not be loaded. Data at the root level is invalid. Line 1, position 1.
  https://github.com/aspnet/JavaScriptServices/issues/495
  https://chocolatey.org/packages/dotnetcore-sdk
  choco install dotnetcore-sdk

https://code.visualstudio.com/docs/cpp/config-msvc


## Este funciona en un hello world!
cl.exe Main.cpp /Zi /EHsc /Fe: D:\projects\cpp\vector\Build\Main.exe 
.\Build\Main.exe

## Este es el que tiene los includes
cl.exe TutorialPlugin.cpp /I "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/include" /c /EHsc /Fe: D:\projects\cpp\rl-plugin-tutorial\Build\Main.exe /link /LIBPATH:"C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/lib"

Dice que hace falta el main, entonces:
https://stackoverflow.com/a/17228890

## Errores de linkeo
https://gist.github.com/jsanchezuy/23b1fc8c592455f1bb84
cl.exe TutorialPlugin.cpp /I "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/include" /c /EHsc
lib TutorialPlugin.obj

https://stackoverflow.com/a/2220213

lib /out:libbgi.lib *.obj
link /DLL TutorialPlugin.obj


lib /out:libbgi.lib *.obj
link /DLL /out:bgi.dll *.obj "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/lib/bakkesmod.lib"

https://stackoverflow.com/questions/2548138/how-to-compile-x64-code-with-visual-studio-in-command-line
warning LNK4272: library machine type 'x64' conflicts with target machine type 'x86'



choco install visualcppbuildtools




## Commands
call "C:\Program Files (x86)\Microsoft Visual C++ Build Tools\vcbuildtools.bat" amd64_x86
cd /D "D:\projects\cpp\TutorialPlugin"
mkdir Build
cl.exe TutorialPlugin.cpp /I "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/include" /c /EHsc /Fo.\Build\
lib /out:./Build/TutorialPlugin.lib ./Build/TutorialPlugin.obj
link /DLL /out:./Build/TutorialPlugin.dll ./Build/TutorialPlugin.obj "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/lib/bakkesmod.lib"
