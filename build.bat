@ECHO off

mkdir Build

cl.exe TutorialPlugin.cpp /I "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/include" /c /EHsc /Fo.\Build\
lib /out:./Build/TutorialPlugin.lib ./Build/TutorialPlugin.obj
link /DLL /out:./Build/TutorialPlugin.dll ./Build/TutorialPlugin.obj "C:/Program Files (x86)/Steam/steamapps/common/rocketleague/Binaries/Win64/bakkesmod/bakkesmodsdk/lib/bakkesmod.lib"

call bakkesmod-patch.exe ./Build/TutorialPlugin.dll
